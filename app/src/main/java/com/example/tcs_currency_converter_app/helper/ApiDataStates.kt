package com.example.tcs_currency_converter_app.helper

data class ApiDataStates <out T>(val status: Status, val data: T?, val message: String?) {

    enum class Status {
        SUCCESS,
        ERROR
    }

    companion object {
        fun <T> success(data: T): ApiDataStates<T> {
            return ApiDataStates(Status.SUCCESS, data, null)
        }

        fun <T> error(message: String, data: T? = null): ApiDataStates<T> {
            return ApiDataStates(Status.ERROR, data, message)
        }
    }
}
