package com.example.tcs_currency_converter_app.networking

import javax.inject.Inject

class ApiExpose @Inject constructor(private val apiService: ApiService) {
    suspend fun getData()  = apiService.getCurrencyResponse()
}