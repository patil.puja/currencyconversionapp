package com.example.tcs_currency_converter_app.networking

import android.util.Log
import com.example.tcs_currency_converter_app.helper.ApiDataStates
import retrofit2.Response
import java.lang.Exception

abstract class ApiDataSource {

    suspend fun <T> safeApiCall(apiCall: suspend () -> Response<T>): ApiDataStates<T> {

        try {
            val response = apiCall()
            if (response.isSuccessful) {
                val body = response.body()
                if (body != null) {
                    return ApiDataStates.success(body)
                }
            }
            return error("${response.code()} ${response.message()}")
        } catch (e: Exception) {
            return error(e.message ?: e.toString())
        }
    }

    private fun <T> error(message: String): ApiDataStates<T> {
        Log.e("remoteDataSource", message)
        return ApiDataStates.error("Network call has failed for a following reason: $message")
    }
}