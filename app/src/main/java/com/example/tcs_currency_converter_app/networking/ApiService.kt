package com.example.tcs_currency_converter_app.networking

import com.example.tcs_currency_converter_app.model.CurrencyResponse
import retrofit2.Response
import retrofit2.http.GET

interface ApiService {

    @GET ("getJsonRates.wbc.fx.json")
    suspend fun getCurrencyResponse() : Response<CurrencyResponse>

}