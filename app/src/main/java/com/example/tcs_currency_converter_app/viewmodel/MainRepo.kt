package com.example.tcs_currency_converter_app.viewmodel

import com.example.tcs_currency_converter_app.helper.ApiDataStates
import com.example.tcs_currency_converter_app.model.CurrencyResponse
import com.example.tcs_currency_converter_app.networking.ApiDataSource
import com.example.tcs_currency_converter_app.networking.ApiExpose
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import javax.inject.Inject

class MainRepo  @Inject constructor(private val apiExpose: ApiExpose) : ApiDataSource() {

    //Using coroutines flow to get the response from
    suspend fun getCurrencyData(): Flow<ApiDataStates<CurrencyResponse>> {
        return flow {
            emit(safeApiCall { apiExpose.getData() })
        }.flowOn(Dispatchers.IO)
    }
}