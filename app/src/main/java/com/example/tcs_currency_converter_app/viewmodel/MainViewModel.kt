package com.example.tcs_currency_converter_app.viewmodel

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch
import com.example.tcs_currency_converter_app.helper.ApiDataStates
import com.example.tcs_currency_converter_app.helper.LiveDataEvent
import com.example.tcs_currency_converter_app.model.CurrencyResponse
import kotlinx.coroutines.flow.collect


class MainViewModel @ViewModelInject constructor(private val mainRepo: MainRepo) : ViewModel()  {

    private val data = LiveDataEvent<ApiDataStates<CurrencyResponse>>()

    //public
    val dataEvent  =  data

    //Public function to get the result of conversion
    fun getResponse() {
        viewModelScope.launch {
            mainRepo.getCurrencyData().collect {
                dataEvent.value = it
            }
        }
    }
}