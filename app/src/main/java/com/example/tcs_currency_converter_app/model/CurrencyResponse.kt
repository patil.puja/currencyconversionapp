package com.example.tcs_currency_converter_app.model

data class CurrencyResponse(
    val apiVersion: String,
    val `data`: Data,
    val status: Int
)

data class Data(
    val Brands: Brands
)

data class Brands(
    val WBC: WBC
)

data class WBC(
    val Brand: String,
    val Portfolios: Portfolios
)

data class Portfolios(
    val FX: FX
)

data class FX(
    val PortfolioId: String,
    val Products: Products
)

data class Products(
    val AED: AED,
    val ARS: ARS,
    val BDT: BDT,
    val BND: BND,
    val BRL: BRL,
    val CAD: CAD,
    val CHF: CHF,
    val CLP: CLP,
    val CNH: CNH,
    val CNY: CNY,
    val DKK: DKK,
    val EUR: EUR,
    val FJD: FJD,
    val GBP: GBP,
    val HKD: HKD,
    val IDR: IDR,
    val INR: INR,
    val JPY: JPY,
    val KRW: KRW,
    val LKR: LKR,
    val MYR: MYR,
    val NOK: NOK,
    val NZD: NZD,
    val PGK: PGK,
    val PHP: PHP,
    val PKR: PKR,
    val SAR: SAR,
    val SBD: SBD,
    val SEK: SEK,
    val SGD: SGD,
    val THB: THB,
    val TOP: TOP,
    val TWD: TWD,
    val USD: USD,
    val VND: VND,
    val VUV: VUV,
    val WST: WST,
    val XAU: XAU,
    val XPF: XPF,
    val ZAR: ZAR
)

data class AED(
    val ProductId: String,
    val Rates: Rates
)

data class ARS(
    val ProductId: String,
    val Rates: Rates1
)

data class BDT(
    val ProductId: String,
    val Rates: Rates2
)

data class BND(
    val ProductId: String,
    val Rates: Rates3
)

data class BRL(
    val ProductId: String,
    val Rates: Rates4
)

data class CAD(
    val ProductId: String,
    val Rates: Rates5
)

data class CHF(
    val ProductId: String,
    val Rates: Rates6
)

data class CLP(
    val ProductId: String,
    val Rates: Rates7
)

data class CNH(
    val ProductId: String,
    val Rates: Rates8
)

data class CNY(
    val ProductId: String,
    val Rates: Rates9
)

data class DKK(
    val ProductId: String,
    val Rates: Rates10
)

data class EUR(
    val ProductId: String,
    val Rates: Rates11
)

data class FJD(
    val ProductId: String,
    val Rates: Rates12
)

data class GBP(
    val ProductId: String,
    val Rates: Rates13
)

data class HKD(
    val ProductId: String,
    val Rates: Rates14
)

data class IDR(
    val ProductId: String,
    val Rates: Rates15
)

data class INR(
    val ProductId: String,
    val Rates: Rates16
)

data class JPY(
    val ProductId: String,
    val Rates: Rates17
)

data class KRW(
    val ProductId: String,
    val Rates: Rates18
)

data class LKR(
    val ProductId: String,
    val Rates: Rates19
)

data class MYR(
    val ProductId: String,
    val Rates: Rates20
)

data class NOK(
    val ProductId: String,
    val Rates: Rates21
)

data class NZD(
    val ProductId: String,
    val Rates: Rates22
)

data class PGK(
    val ProductId: String,
    val Rates: Rates23
)

data class PHP(
    val ProductId: String,
    val Rates: Rates24
)

data class PKR(
    val ProductId: String,
    val Rates: Rates25
)

data class SAR(
    val ProductId: String,
    val Rates: Rates26
)

data class SBD(
    val ProductId: String,
    val Rates: Rates27
)

data class SEK(
    val ProductId: String,
    val Rates: Rates28
)

data class SGD(
    val ProductId: String,
    val Rates: Rates29
)

data class THB(
    val ProductId: String,
    val Rates: Rates30
)

data class TOP(
    val ProductId: String,
    val Rates: Rates31
)

data class TWD(
    val ProductId: String,
    val Rates: Rates32
)

data class USD(
    val ProductId: String,
    val Rates: Rates33
)

data class VND(
    val ProductId: String,
    val Rates: Rates34
)

data class VUV(
    val ProductId: String,
    val Rates: Rates35
)

data class WST(
    val ProductId: String,
    val Rates: Rates36
)

data class XAU(
    val ProductId: String,
    val Rates: Rates37
)

data class XPF(
    val ProductId: String,
    val Rates: Rates38
)

data class ZAR(
    val ProductId: String,
    val Rates: Rates39
)

data class Rates(
    val AED: AEDX
)

data class AEDX(
    val LASTUPDATED: String,
    val SpotRate_Date_Fmt: String,
    val buyNotes: String,
    val buyTC: String,
    val buyTT: String,
    val country: String,
    val currencyCode: String,
    val currencyName: String,
    val effectiveDate_Fmt: String,
    val sellNotes: String,
    val sellTT: String,
    val updateDate_Fmt: String
)

data class Rates1(
    val ARS: ARSX
)

data class ARSX(
    val LASTUPDATED: String,
    val SpotRate_Date_Fmt: String,
    val buyNotes: String,
    val buyTC: String,
    val buyTT: String,
    val country: String,
    val currencyCode: String,
    val currencyName: String,
    val effectiveDate_Fmt: String,
    val sellNotes: String,
    val sellTT: String,
    val updateDate_Fmt: String
)

data class Rates2(
    val BDT: BDTX
)

data class BDTX(
    val LASTUPDATED: String,
    val SpotRate_Date_Fmt: String,
    val buyNotes: String,
    val buyTC: String,
    val buyTT: String,
    val country: String,
    val currencyCode: String,
    val currencyName: String,
    val effectiveDate_Fmt: String,
    val sellNotes: String,
    val sellTT: String,
    val updateDate_Fmt: String
)

data class Rates3(
    val BND: BNDX
)

data class BNDX(
    val LASTUPDATED: String,
    val SpotRate_Date_Fmt: String,
    val buyNotes: String,
    val buyTC: String,
    val buyTT: String,
    val country: String,
    val currencyCode: String,
    val currencyName: String,
    val effectiveDate_Fmt: String,
    val sellNotes: String,
    val sellTT: String,
    val updateDate_Fmt: String
)

data class Rates4(
    val BRL: BRLX
)

data class BRLX(
    val LASTUPDATED: String,
    val SpotRate_Date_Fmt: String,
    val buyNotes: String,
    val buyTC: String,
    val buyTT: String,
    val country: String,
    val currencyCode: String,
    val currencyName: String,
    val effectiveDate_Fmt: String,
    val sellNotes: String,
    val sellTT: String,
    val updateDate_Fmt: String
)

data class Rates5(
    val CAD: CADX
)

data class CADX(
    val LASTUPDATED: String,
    val SpotRate_Date_Fmt: String,
    val buyNotes: String,
    val buyTC: String,
    val buyTT: String,
    val country: String,
    val currencyCode: String,
    val currencyName: String,
    val effectiveDate_Fmt: String,
    val sellNotes: String,
    val sellTT: String,
    val updateDate_Fmt: String
)

data class Rates6(
    val CHF: CHFX
)

data class CHFX(
    val LASTUPDATED: String,
    val SpotRate_Date_Fmt: String,
    val buyNotes: String,
    val buyTC: String,
    val buyTT: String,
    val country: String,
    val currencyCode: String,
    val currencyName: String,
    val effectiveDate_Fmt: String,
    val sellNotes: String,
    val sellTT: String,
    val updateDate_Fmt: String
)

data class Rates7(
    val CLP: CLPX
)

data class CLPX(
    val LASTUPDATED: String,
    val SpotRate_Date_Fmt: String,
    val buyNotes: String,
    val buyTC: String,
    val buyTT: String,
    val country: String,
    val currencyCode: String,
    val currencyName: String,
    val effectiveDate_Fmt: String,
    val sellNotes: String,
    val sellTT: String,
    val updateDate_Fmt: String
)

data class Rates8(
    val CNH: CNHX
)

data class CNHX(
    val LASTUPDATED: String,
    val SpotRate_Date_Fmt: String,
    val buyNotes: String,
    val buyTC: String,
    val buyTT: String,
    val country: String,
    val currencyCode: String,
    val currencyName: String,
    val effectiveDate_Fmt: String,
    val sellNotes: String,
    val sellTT: String,
    val updateDate_Fmt: String
)

data class Rates9(
    val CNY: CNYX
)

data class CNYX(
    val LASTUPDATED: String,
    val SpotRate_Date_Fmt: String,
    val buyNotes: String,
    val buyTC: String,
    val buyTT: String,
    val country: String,
    val currencyCode: String,
    val currencyName: String,
    val effectiveDate_Fmt: String,
    val sellNotes: String,
    val sellTT: String,
    val updateDate_Fmt: String
)

data class Rates10(
    val DKK: DKKX
)

data class DKKX(
    val LASTUPDATED: String,
    val SpotRate_Date_Fmt: String,
    val buyNotes: String,
    val buyTC: String,
    val buyTT: String,
    val country: String,
    val currencyCode: String,
    val currencyName: String,
    val effectiveDate_Fmt: String,
    val sellNotes: String,
    val sellTT: String,
    val updateDate_Fmt: String
)

data class Rates11(
    val EUR: EURX
)

data class EURX(
    val LASTUPDATED: String,
    val SpotRate_Date_Fmt: String,
    val buyNotes: String,
    val buyTC: String,
    val buyTT: String,
    val country: String,
    val currencyCode: String,
    val currencyName: String,
    val effectiveDate_Fmt: String,
    val sellNotes: String,
    val sellTT: String,
    val updateDate_Fmt: String
)

data class Rates12(
    val FJD: FJDX
)

data class FJDX(
    val LASTUPDATED: String,
    val SpotRate_Date_Fmt: String,
    val buyNotes: String,
    val buyTC: String,
    val buyTT: String,
    val country: String,
    val currencyCode: String,
    val currencyName: String,
    val effectiveDate_Fmt: String,
    val sellNotes: String,
    val sellTT: String,
    val updateDate_Fmt: String
)

data class Rates13(
    val GBP: GBPX
)

data class GBPX(
    val LASTUPDATED: String,
    val SpotRate_Date_Fmt: String,
    val buyNotes: String,
    val buyTC: String,
    val buyTT: String,
    val country: String,
    val currencyCode: String,
    val currencyName: String,
    val effectiveDate_Fmt: String,
    val sellNotes: String,
    val sellTT: String,
    val updateDate_Fmt: String
)

data class Rates14(
    val HKD: HKDX
)

data class HKDX(
    val LASTUPDATED: String,
    val SpotRate_Date_Fmt: String,
    val buyNotes: String,
    val buyTC: String,
    val buyTT: String,
    val country: String,
    val currencyCode: String,
    val currencyName: String,
    val effectiveDate_Fmt: String,
    val sellNotes: String,
    val sellTT: String,
    val updateDate_Fmt: String
)

data class Rates15(
    val IDR: IDRX
)

data class IDRX(
    val LASTUPDATED: String,
    val SpotRate_Date_Fmt: String,
    val buyNotes: String,
    val buyTC: String,
    val buyTT: String,
    val country: String,
    val currencyCode: String,
    val currencyName: String,
    val effectiveDate_Fmt: String,
    val sellNotes: String,
    val sellTT: String,
    val updateDate_Fmt: String
)

data class Rates16(
    val INR: INRX
)

data class INRX(
    val LASTUPDATED: String,
    val SpotRate_Date_Fmt: String,
    val buyNotes: String,
    val buyTC: String,
    val buyTT: String,
    val country: String,
    val currencyCode: String,
    val currencyName: String,
    val effectiveDate_Fmt: String,
    val sellNotes: String,
    val sellTT: String,
    val updateDate_Fmt: String
)

data class Rates17(
    val JPY: JPYX
)

data class JPYX(
    val LASTUPDATED: String,
    val SpotRate_Date_Fmt: String,
    val buyNotes: String,
    val buyTC: String,
    val buyTT: String,
    val country: String,
    val currencyCode: String,
    val currencyName: String,
    val effectiveDate_Fmt: String,
    val sellNotes: String,
    val sellTT: String,
    val updateDate_Fmt: String
)

data class Rates18(
    val KRW: KRWX
)

data class KRWX(
    val LASTUPDATED: String,
    val SpotRate_Date_Fmt: String,
    val buyNotes: String,
    val buyTC: String,
    val buyTT: String,
    val country: String,
    val currencyCode: String,
    val currencyName: String,
    val effectiveDate_Fmt: String,
    val sellNotes: String,
    val sellTT: String,
    val updateDate_Fmt: String
)

data class Rates19(
    val LKR: LKRX
)

data class LKRX(
    val LASTUPDATED: String,
    val SpotRate_Date_Fmt: String,
    val buyNotes: String,
    val buyTC: String,
    val buyTT: String,
    val country: String,
    val currencyCode: String,
    val currencyName: String,
    val effectiveDate_Fmt: String,
    val sellNotes: String,
    val sellTT: String,
    val updateDate_Fmt: String
)

data class Rates20(
    val MYR: MYRX
)

data class MYRX(
    val LASTUPDATED: String,
    val SpotRate_Date_Fmt: String,
    val buyNotes: String,
    val buyTC: String,
    val buyTT: String,
    val country: String,
    val currencyCode: String,
    val currencyName: String,
    val effectiveDate_Fmt: String,
    val sellNotes: String,
    val sellTT: String,
    val updateDate_Fmt: String
)

data class Rates21(
    val NOK: NOKX
)

data class NOKX(
    val LASTUPDATED: String,
    val SpotRate_Date_Fmt: String,
    val buyNotes: String,
    val buyTC: String,
    val buyTT: String,
    val country: String,
    val currencyCode: String,
    val currencyName: String,
    val effectiveDate_Fmt: String,
    val sellNotes: String,
    val sellTT: String,
    val updateDate_Fmt: String
)

data class Rates22(
    val NZD: NZDX
)

data class NZDX(
    val LASTUPDATED: String,
    val SpotRate_Date_Fmt: String,
    val buyNotes: String,
    val buyTC: String,
    val buyTT: String,
    val country: String,
    val currencyCode: String,
    val currencyName: String,
    val effectiveDate_Fmt: String,
    val sellNotes: String,
    val sellTT: String,
    val updateDate_Fmt: String
)

data class Rates23(
    val PGK: PGKX
)

data class PGKX(
    val LASTUPDATED: String,
    val SpotRate_Date_Fmt: String,
    val buyNotes: String,
    val buyTC: String,
    val buyTT: String,
    val country: String,
    val currencyCode: String,
    val currencyName: String,
    val effectiveDate_Fmt: String,
    val sellNotes: String,
    val sellTT: String,
    val updateDate_Fmt: String
)

data class Rates24(
    val PHP: PHPX
)

data class PHPX(
    val LASTUPDATED: String,
    val SpotRate_Date_Fmt: String,
    val buyNotes: String,
    val buyTC: String,
    val buyTT: String,
    val country: String,
    val currencyCode: String,
    val currencyName: String,
    val effectiveDate_Fmt: String,
    val sellNotes: String,
    val sellTT: String,
    val updateDate_Fmt: String
)

data class Rates25(
    val PKR: PKRX
)

data class PKRX(
    val LASTUPDATED: String,
    val SpotRate_Date_Fmt: String,
    val buyNotes: String,
    val buyTC: String,
    val buyTT: String,
    val country: String,
    val currencyCode: String,
    val currencyName: String,
    val effectiveDate_Fmt: String,
    val sellNotes: String,
    val sellTT: String,
    val updateDate_Fmt: String
)

data class Rates26(
    val SAR: SARX
)

data class SARX(
    val LASTUPDATED: String,
    val SpotRate_Date_Fmt: String,
    val buyNotes: String,
    val buyTC: String,
    val buyTT: String,
    val country: String,
    val currencyCode: String,
    val currencyName: String,
    val effectiveDate_Fmt: String,
    val sellNotes: String,
    val sellTT: String,
    val updateDate_Fmt: String
)

data class Rates27(
    val SBD: SBDX
)

data class SBDX(
    val LASTUPDATED: String,
    val SpotRate_Date_Fmt: String,
    val buyNotes: String,
    val buyTC: String,
    val buyTT: String,
    val country: String,
    val currencyCode: String,
    val currencyName: String,
    val effectiveDate_Fmt: String,
    val sellNotes: String,
    val sellTT: String,
    val updateDate_Fmt: String
)

data class Rates28(
    val SEK: SEKX
)

data class SEKX(
    val LASTUPDATED: String,
    val SpotRate_Date_Fmt: String,
    val buyNotes: String,
    val buyTC: String,
    val buyTT: String,
    val country: String,
    val currencyCode: String,
    val currencyName: String,
    val effectiveDate_Fmt: String,
    val sellNotes: String,
    val sellTT: String,
    val updateDate_Fmt: String
)

data class Rates29(
    val SGD: SGDX
)

data class SGDX(
    val LASTUPDATED: String,
    val SpotRate_Date_Fmt: String,
    val buyNotes: String,
    val buyTC: String,
    val buyTT: String,
    val country: String,
    val currencyCode: String,
    val currencyName: String,
    val effectiveDate_Fmt: String,
    val sellNotes: String,
    val sellTT: String,
    val updateDate_Fmt: String
)

data class Rates30(
    val THB: THBX
)

data class THBX(
    val LASTUPDATED: String,
    val SpotRate_Date_Fmt: String,
    val buyNotes: String,
    val buyTC: String,
    val buyTT: String,
    val country: String,
    val currencyCode: String,
    val currencyName: String,
    val effectiveDate_Fmt: String,
    val sellNotes: String,
    val sellTT: String,
    val updateDate_Fmt: String
)

data class Rates31(
    val TOP: TOPX
)

data class TOPX(
    val LASTUPDATED: String,
    val SpotRate_Date_Fmt: String,
    val buyNotes: String,
    val buyTC: String,
    val buyTT: String,
    val country: String,
    val currencyCode: String,
    val currencyName: String,
    val effectiveDate_Fmt: String,
    val sellNotes: String,
    val sellTT: String,
    val updateDate_Fmt: String
)

data class Rates32(
    val TWD: TWDX
)

data class TWDX(
    val LASTUPDATED: String,
    val SpotRate_Date_Fmt: String,
    val buyNotes: String,
    val buyTC: String,
    val buyTT: String,
    val country: String,
    val currencyCode: String,
    val currencyName: String,
    val effectiveDate_Fmt: String,
    val sellNotes: String,
    val sellTT: String,
    val updateDate_Fmt: String
)

data class Rates33(
    val USD: USDX
)

data class USDX(
    val LASTUPDATED: String,
    val SpotRate_Date_Fmt: String,
    val buyNotes: String,
    val buyTC: String,
    val buyTT: String,
    val country: String,
    val currencyCode: String,
    val currencyName: String,
    val effectiveDate_Fmt: String,
    val sellNotes: String,
    val sellTT: String,
    val updateDate_Fmt: String
)

data class Rates34(
    val VND: VNDX
)

data class VNDX(
    val LASTUPDATED: String,
    val SpotRate_Date_Fmt: String,
    val buyNotes: String,
    val buyTC: String,
    val buyTT: String,
    val country: String,
    val currencyCode: String,
    val currencyName: String,
    val effectiveDate_Fmt: String,
    val sellNotes: String,
    val sellTT: String,
    val updateDate_Fmt: String
)

data class Rates35(
    val VUV: VUVX
)

data class VUVX(
    val LASTUPDATED: String,
    val SpotRate_Date_Fmt: String,
    val buyNotes: String,
    val buyTC: String,
    val buyTT: String,
    val country: String,
    val currencyCode: String,
    val currencyName: String,
    val effectiveDate_Fmt: String,
    val sellNotes: String,
    val sellTT: String,
    val updateDate_Fmt: String
)

data class Rates36(
    val WST: WSTX
)

data class WSTX(
    val LASTUPDATED: String,
    val SpotRate_Date_Fmt: String,
    val buyNotes: String,
    val buyTC: String,
    val buyTT: String,
    val country: String,
    val currencyCode: String,
    val currencyName: String,
    val effectiveDate_Fmt: String,
    val sellNotes: String,
    val sellTT: String,
    val updateDate_Fmt: String
)

data class Rates37(
    val XAU: XAUX
)

data class XAUX(
    val LASTUPDATED: String,
    val SpotRate_Date_Fmt: String,
    val buyNotes: String,
    val buyTC: String,
    val buyTT: String,
    val country: String,
    val currencyCode: String,
    val currencyName: String,
    val effectiveDate_Fmt: String,
    val sellNotes: String,
    val sellTT: String,
    val updateDate_Fmt: String
)

data class Rates38(
    val XPF: XPFX
)

data class XPFX(
    val LASTUPDATED: String,
    val SpotRate_Date_Fmt: String,
    val buyNotes: String,
    val buyTC: String,
    val buyTT: String,
    val country: String,
    val currencyCode: String,
    val currencyName: String,
    val effectiveDate_Fmt: String,
    val sellNotes: String,
    val sellTT: String,
    val updateDate_Fmt: String
)

data class Rates39(
    val ZAR: ZARX
)

data class ZARX(
    val LASTUPDATED: String,
    val SpotRate_Date_Fmt: String,
    val buyNotes: String,
    val buyTC: String,
    val buyTT: String,
    val country: String,
    val currencyCode: String,
    val currencyName: String,
    val effectiveDate_Fmt: String,
    val sellNotes: String,
    val sellTT: String,
    val updateDate_Fmt: String
)