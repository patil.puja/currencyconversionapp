package com.example.tcs_currency_converter_app.view

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.View
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.example.tcs_currency_converter_app.R
import com.example.tcs_currency_converter_app.databinding.ActivityMainBinding
import com.example.tcs_currency_converter_app.helper.ApiDataStates
import com.example.tcs_currency_converter_app.helper.Utils
import com.example.tcs_currency_converter_app.model.CurrencyResponse
import com.example.tcs_currency_converter_app.viewmodel.MainViewModel
import com.google.android.material.snackbar.Snackbar
import com.google.gson.Gson
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.activity_main.*
import org.json.JSONObject
import kotlin.collections.ArrayList

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    //viewBinding to get the reference of the views
    private var dataBinding : ActivityMainBinding? = null
    private val binding get() = dataBinding!!

    private val mainViewModel: MainViewModel by viewModels()

    private val currencyList = ArrayList<String>()
    private val rateList = ArrayList<String>()
    var selectedCurrencyPosition  : Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        dataBinding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        initSpinner()
        observeUi()
        setCurrencyConvertClickListener()
    }

    private fun setCurrencyConvertClickListener() {
        dataBinding?.btnCurrencyConvert?.setOnClickListener() {
            val numberToConvert = binding.etFirstCurrency.text.toString()
            if(numberToConvert.isEmpty() || numberToConvert == "0"){
                Snackbar.make(binding.mainLayout,"Please enter amount", Snackbar.LENGTH_LONG)
                    .setTextColor(ContextCompat.getColor(this, R.color.white))
                    .show()
            }else{
                val currencyRate = rateList[selectedCurrencyPosition]
                val convertedRate = numberToConvert.toDouble() / currencyRate.toDouble()
                dataBinding?.txtResult?.text = String.format("%,.2f", convertedRate)
                etFirstCurrency.text.clear()
                binding.lytResult.visibility = View.VISIBLE
            }
        }
    }

    private fun initSpinner() {
        if (Utils.isNetworkAvailable(this)){
            mainViewModel.getResponse()
        } else{
            Snackbar.make(binding.mainLayout,"You are not connected to the internet", Snackbar.LENGTH_LONG)
                .setTextColor(ContextCompat.getColor(this, R.color.white))
                .show()
        }

        val spinner = binding.spnCurrency
        spinner.setItems(currencyList)
        spinner.setOnClickListener(){
            binding.lytResult.visibility = View.INVISIBLE
        }
        spinner.setOnItemSelectedListener { view, position, id, item ->
            selectedCurrencyPosition = position
        }
    }

    @SuppressLint("SetTextI18n")
    private fun observeUi() {
        mainViewModel.dataEvent.observe(this, androidx.lifecycle.Observer { result ->

            when(result.status){
                ApiDataStates.Status.SUCCESS -> {
                    getCurrencyList(result)
                }
                ApiDataStates.Status.ERROR -> {
                    val layout = binding.mainLayout
                    Snackbar.make(layout,"" , Snackbar.LENGTH_LONG)
                    Snackbar.make(layout,  "Oopps! Something went wrong, Try again", Snackbar.LENGTH_LONG)
                        .setTextColor(ContextCompat.getColor(this, R.color.black))
                        .show()
                    binding.btnCurrencyConvert.visibility = View.VISIBLE
                }
            }
        })
    }

    private fun getCurrencyList(result: ApiDataStates<CurrencyResponse>)  {
        val dataList = result.data?.data?.Brands?.WBC?.Portfolios?.FX?.Products
        val gson = Gson()
        val jsonData: String = gson.toJson(dataList)
        val jsonObject = JSONObject(jsonData)
        val itemList : Iterator<String> = jsonObject.keys()

        for(item in itemList) {
            val key = itemList.next()
            val currency = jsonObject.getJSONObject(key).getJSONObject("Rates")
                .getJSONObject(key).optString("currencyCode")

            val rate = jsonObject.getJSONObject(key).getJSONObject("Rates")
                .getJSONObject(key).optString("buyTT");

            currencyList.add(currency)
            rateList.add(rate)
        }
    }

}
